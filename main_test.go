package main

import (
	"testing"

	dns "github.com/cert-manager/cert-manager/test/acme"
)

func TestRunsSuite(t *testing.T) {

	fixture := dns.NewFixture(&nirvatiMeDnsSolver{},
		dns.SetResolvedZone("staging.nirvati.me."),
		dns.SetAllowAmbientCredentials(false),
		dns.SetManifestPath("testdata"),
	)

	fixture.RunBasic(t)
	fixture.RunExtended(t)
}
