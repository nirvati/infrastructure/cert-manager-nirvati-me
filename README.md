# nirvati.me cert-manager integration

This is a [cert-manager](https://cert-manager.io/) integration for [nirvati.me](https://nirvati.me/), which allows you to automatically provision and renew TLS certificates for your nirvati.me subdomain without a public IP address.
