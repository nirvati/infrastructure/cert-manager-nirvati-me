---
# Create a selfsigned Issuer, in order to create a root CA certificate for
# signing webhook serving certificates
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: {{ include "cert-manager-nirvati-me.selfSignedIssuer" . }}
  namespace: {{ .Release.Namespace | quote }}
  labels:
    app: {{ include "cert-manager-nirvati-me.name" . }}
    chart: {{ include "cert-manager-nirvati-me.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  selfSigned: {}

---

# Generate a CA Certificate used to sign certificates for the webhook
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: {{ include "cert-manager-nirvati-me.rootCACertificate" . }}
  namespace: {{ .Release.Namespace | quote }}
  labels:
    app: {{ include "cert-manager-nirvati-me.name" . }}
    chart: {{ include "cert-manager-nirvati-me.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  secretName: {{ include "cert-manager-nirvati-me.rootCACertificate" . }}
  duration: 43800h # 5y
  issuerRef:
    name: {{ include "cert-manager-nirvati-me.selfSignedIssuer" . }}
  commonName: "ca.cert-manager-nirvati-me.cert-manager"
  isCA: true

---

# Create an Issuer that uses the above generated CA certificate to issue certs
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: {{ include "cert-manager-nirvati-me.rootCAIssuer" . }}
  namespace: {{ .Release.Namespace | quote }}
  labels:
    app: {{ include "cert-manager-nirvati-me.name" . }}
    chart: {{ include "cert-manager-nirvati-me.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  ca:
    secretName: {{ include "cert-manager-nirvati-me.rootCACertificate" . }}

---

# Finally, generate a serving certificate for the webhook to use
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: {{ include "cert-manager-nirvati-me.servingCertificate" . }}
  namespace: {{ .Release.Namespace | quote }}
  labels:
    app: {{ include "cert-manager-nirvati-me.name" . }}
    chart: {{ include "cert-manager-nirvati-me.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  secretName: {{ include "cert-manager-nirvati-me.servingCertificate" . }}
  duration: 8760h # 1y
  issuerRef:
    name: {{ include "cert-manager-nirvati-me.rootCAIssuer" . }}
  dnsNames:
  - {{ include "cert-manager-nirvati-me.fullname" . }}
  - {{ include "cert-manager-nirvati-me.fullname" . }}.{{ .Release.Namespace }}
  - {{ include "cert-manager-nirvati-me.fullname" . }}.{{ .Release.Namespace }}.svc
